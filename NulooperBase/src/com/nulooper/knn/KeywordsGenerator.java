package com.nulooper.knn;

import java.io.IOException;
import java.io.StringReader;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.lucene.analysis.ASCIIFoldingFilter;
import org.apache.lucene.analysis.LowerCaseFilter;
import org.apache.lucene.analysis.PorterStemFilter;
import org.apache.lucene.analysis.StopFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.ClassicFilter;
import org.apache.lucene.analysis.standard.ClassicTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Guesses keywords from an input string, based on the frequency of the words.
 * 
 * @see <a href="http://lucene.apache.org/">http://lucene.apache.org/</a>
 */
public class KeywordsGenerator {

	/** Lucene version. */
	private static Version LUCENE_VERSION = Version.LUCENE_36;

	/**
	 * Stemmize the given term.
	 * 
	 * @param term
	 *            The term to stem.
	 * @return The stem of the given term.
	 * @throws IOException
	 *             If an I/O error occured.
	 */
	private static String stemmize(String term) throws IOException {

		// tokenize term
		TokenStream tokenStream = new ClassicTokenizer(LUCENE_VERSION,
				new StringReader(term));
		// stemmize
		tokenStream = new PorterStemFilter(tokenStream);

		Set<String> stems = new HashSet<String>();
		CharTermAttribute token = tokenStream
				.getAttribute(CharTermAttribute.class);
		// for each token
		while (tokenStream.incrementToken()) {
			// add it in the dedicated set (to keep unicity)
			stems.add(token.toString());
		}

		// if no stem or 2+ stems have been found, return null
		if (stems.size() != 1) {
			return null;
		}

		String stem = stems.iterator().next();

		// if the stem has non-alphanumerical chars, return null
		if (!stem.matches("[\\w-]+")) {
			return null;
		}

		return stem;
	}

	/**
	 * Tries to find the given example within the given collection. If it hasn't
	 * been found, the example is automatically added in the collection and is
	 * then returned.
	 * 
	 * @param collection
	 *            The collection to search into.
	 * @param example
	 *            The example to search.
	 * @return The existing element if it has been found, the given example
	 *         otherwise.
	 */
	private static <T> T find(Collection<T> collection, T example) {
		for (T element : collection) {
			if (element.equals(example)) {
				return element;
			}
		}
		collection.add(example);
		return example;
	}

	/**
	 * Extracts text content from the given URL and guesses keywords within it
	 * (needs jsoup parser).
	 * 
	 * @param The
	 *            URL to read.
	 * @return A set of potential keywords. The first keyword is the most
	 *         frequent one, the last the least frequent.
	 * @throws IOException
	 *             If an I/O error occured.
	 * @see <a href="http://jsoup.org/">http://jsoup.org/</a>
	 */
	public static List<Keyword> guessFromUrl(String url) throws IOException {
		// get textual content from url
		Document doc = Jsoup.connect(url).get();
		String content = doc.body().text();
		// guess keywords from this content
		return guessFromString(content);
	}

	/**
	 * Guesses keywords from given input string.
	 * 
	 * @param input
	 *            The input string.
	 * @return A set of potential keywords. The first keyword is the most
	 *         frequent one, the last the least frequent.
	 * @throws IOException
	 *             If an I/O error occured.
	 */
	public static List<Keyword> guessFromString(String input)
			throws IOException {

		// hack to keep dashed words (e.g. "non-specific" rather than "non" and
		// "specific")
		input = input.replaceAll("-+", "-0");
		// replace any punctuation char but dashes and apostrophes and by a
		// space
		input = input.replaceAll("[\\p{Punct}&&[^'-]]+", " ");
		// replace most common english contractions
		input = input.replaceAll("(?:'(?:[tdsm]|[vr]e|ll))+\\b", "");

		// tokenize input
		TokenStream tokenStream = new ClassicTokenizer(LUCENE_VERSION,
				new StringReader(input));
		// to lower case
		tokenStream = new LowerCaseFilter(LUCENE_VERSION, tokenStream);
		// remove dots from acronyms (and "'s" but already done manually above)
		tokenStream = new ClassicFilter(tokenStream);
		// convert any char to ASCII
		tokenStream = new ASCIIFoldingFilter(tokenStream);
		// remove english stop words
		Set<String> stops = new HashSet<String>();
		tokenStream = new StopFilter(LUCENE_VERSION, tokenStream, stops);

		List<Keyword> keywords = new LinkedList<Keyword>();
		CharTermAttribute token = tokenStream
				.getAttribute(CharTermAttribute.class);

		// for each token
		while (tokenStream.incrementToken()) {
			String term = token.toString();
			// stemmize
			String stem = stemmize(term);
			if (stem != null) {
				// create the keyword or get the existing one if any
				Keyword keyword = find(keywords,
						new Keyword(stem.replaceAll("-0", "-")));
				// add its corresponding initial token
				keyword.add(term.replaceAll("-0", "-"));
			}
		}

		// reverse sort by frequency
		Collections.sort(keywords);

		return keywords;
	}

	public static void main(String[] args) throws IOException {
		String text = "IJobs: South Africa: IOL Jobs Job Listings Contact US Feedback About IOL Jobs My Account Job opportunities in South Africa with IOL Jobs. 2,305 Jobs in South Africa Search What (job title, reference or keywords) Where (filter by location) All Locations South Africa - Other International Eastern Cape - All Locations Eastern Cape - Aliwal North Eastern Cape - East London Eastern Cape - Port Elizabeth Eastern Cape - Eastern Cape Other Free State - All Locations Free State - Bethlehem Free State - Bloemfontein Free State - Ficksburg Free State - Harrismith Free State - Sasolburg Free State - Virginia Free State - Welkom Free State - Other Gauteng - All Locations Gauteng - Johannesburg Gauteng - Johannesburg North Gauteng - Johannesburg South Gauteng - Johannesburg East Gauteng - Johannesburg West Gauteng - Midrand Gauteng - Pretoria Gauteng - Centurion Gauteng - other Kwazulu Natal - All Locations Kwazulu Natal - Central / Southern Drakensberg Kwazulu Natal - Durban Kwazulu Natal - Midlands Kwazulu Natal - North Coast Kwazulu Natal - South Coast Kwazulu Natal - Zululand Kwazulu Natal - Other Limpopo - All Locations Limpopo - Pietersburg Limpopo - Polokwane Limpopo - Tzaneen Limpopo - other Mpumalanga - All Locations Mpumalanga - Ermelo Mpumalanga - Hazyview Mpumalanga - Middelburg Mpumalanga - Nelspruit Mpumalanga - Witbank Mpumalanga - Other North West - All Locations North West - Klerksdorp North West - Lichtenburg North West - Mafikeng North West - Potchefstroom North West - Rustenburg North West - Vryburg North West - Other Northern Cape - All Locations Northern Cape - Calvinia Northern Cape - Colesberg Northern Cape - De Aar Northern Cape - Kimberley Northern Cape - Prieska Northern Cape - Upington Northern Cape - Other Western Cape - All Locations Western Cape - Boland Western Cape -Cape Town Centre Western Cape - Garden Route Western Cape - Karoo Western Cape - Overberg Western Cape - West Coast Western Cape - Southern Suburbs Western Cape - Northern Suburbs Western Cape - Other Western Cape - Helderberg Accountancy Banking, Financial Services & Insurance Beauty & Hair Care Jobs Construction & Property Customer Service, Call & Contact Centre Jobs Design & Architecture Education & Training Engineering & Technical Environmental, Health & Safety Jobs Green Jobs Hospitality & Catering HR & Recruitment IT and Telecommunications Legal Leisure & Sports Jobs Management & Executive Jobs Manufacturing, Production & Utilities Jobs Marketing, Advertising & PR Media, New Media & Creative jobs Medical & Healthcare Jobs Pharmaceutical Jobs Public Sector Jobs R&D, Science & Scientific Research Retail & Wholesale Jobs Sales & Purchasing Secretarial, Admin & PA Skilled Labour, Trades & Manual Social, Community & Charity Jobs Transportation, Logistics & Supply Chain Travel & Tourism Jobs Eastern Cape Aliwal North East London Port Elizabeth Other Free State Bethlehem Bloemfontein Ficksburg Harrismith Sasolburg Virginia Welkom Other Gauteng Centurion Johannesburg Johannesburg East Johannesburg North Johannesburg South Johannesburg West Midrand Pretoria Other Kwazulu Natal Central / Southern Drakensberg Durban Midlands North Coast South Coast Zululand Other Limpopo Pietersburg Polokwane Tzaneen Other Mpumalanga Ermelo Hazyview Middelburg Nelspruit Witbank Other North West Klerksdorp Lichtenburg Mafikeng Potchefstroom Rustenburg Vryburg Other Northern Cape Kimberley Calvinia Colesberg De Aar Prieska Upington Other Western Cape Boland Garden Route Karoo Overberg West Coast Cape Town Centre Helderberg Northern Suburbs Southern Suburbs Other SALES MANAGERS   Listed Thu 29 May 2014 SALESMANAGERSNationalcompany seeksCall Dawn on:021 948 7198 /078 198 1245 SALES CONSULANTS   Listed Thu 29 May 2014 SALESCONSULANTSNationalcompany seeksCall Dawn on:021 948 7198 /078 198 1245 CANDIDATE ATTORNEY   Listed Thu 29 May 2014 CANDIDATE ATTORNEYRECRUITMENT PROGRAMME(R141 339  R164 028 paA credible employer of choice rooted in leadership, driven by the value and advancement of human rights. A key contributor to South Africas constitutional democracy ... CASHIERS   Listed Thu 29 May 2014 CASHIERSEEDEDAT BOTTLE STORESHomevale & West EndbranchesDROP off CV atOVERLAND LIQUORSHEUWELSIG Grade A,B And C Security Officers   Listed Thu 29 May 2014 With hotel experience. Send Cv's to 7 Foundrey Road, Salt River. ☎ 021 447 726 Cognitive Behavioural Therapy Intervention   Listed Thu 1 May 2014 The University of Cape Towns Addictions Unit is offering a free 6-session Cognitive Behavioural  Therapy intervention as part of a research study. We are looking for adults 18 and older who are  addicted to TIK/Methamphetamine and who ... Featured Jobs Job Title Company & Location Salary   CellC RICA Agent Listed Fri 16 May 2014 LikeMinds Kwazulu Natal - Midlands Less than R10,000 / Year   Cutter Listed Fri 16 May 2014 Elledi Creations Westen Cape -Cape Town Centre Sales Representative Listed Wed 21 May 2014 Kirk Marketing (PTY) Ltd Gauteng - Johannesburg North R240,001-260,000 / Year   Domestic Worker Listed Thu 22 May 2014 Anmarie Dexter Northern Cape - Kimberley   YOUTH SALES CONSULTANTS Listed Fri 23 May 2014 RECRUIT 5 HOLDINGS Gauteng - other R70,001-80,000 / Year   Quadriplegic's Assistant Listed Wed 28 May 2014 Market Immersion Gauteng - Johannesburg North R40,001-50,000 / Year Click here to download the government Z83 employment application forms Get Email Alerts every time a job of your interest is advertised Advertise with us For exceptional offers in print and on-line - place an ad today. Share or Recomend Facebook Twitter No Iframes ©2012 Independent Online. All rights strictly reserved. Independent Online is a wholly owned subsidiary of Independent News & Media. Reliance on the information this site contains is at your own risk. Please read our Terms and Conditions of Use and Privacy Policy. Contact us here ";
		List<Keyword> keywords = KeywordsGenerator.guessFromString(text);
		System.out.println(keywords.size());
		for (Keyword key : keywords) {
			System.out.println(key.getTerms().size());
		}
	}

}
