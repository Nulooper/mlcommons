package com.nulooper.knn;

import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class KNNClassifier {
	public static void main(String[] args) throws Exception {

		DataSource source = new DataSource("test.arff");
		Instances data = source.getDataSet();
		// setting class attribute if the data format does not provide this
		// information
		// For example, the XRFF format saves the class attribute information as
		// well
		if (data.classIndex() == -1)
			data.setClassIndex(data.numAttributes() - 1);
		NaiveBayes nb = (NaiveBayes) weka.core.SerializationHelper
				.read("oil_bayes.model");
		for (int i = 0; i < data.numInstances(); i++) {
			double clsLabel = nb.classifyInstance(data.instance(i));
			System.out.println(clsLabel);
			System.out.println(clsLabel + " -> "
					+ data.classAttribute().value((int) clsLabel));
		}
	}

}
