package com.nulooper.knn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import com.nulooper.ml.features.Vectorizer;
import com.nulooper.nlp.en.LingPipeStemmer;
import com.nulooper.nlp.en.OpenNLPSentSplitter;
import com.nulooper.nlp.en.OpenNLPTokenizer;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.ArticleSentencesExtractor;
import edu.stanford.nlp.util.StringUtils;

public class Preprocessor {
	private static final Set<String> stopwords = new HashSet<String>();
	static {
		try {
			List<String> lines = FileUtils.readLines(new File(
					"train/stopword.list"));
			for (String word : lines) {
				stopwords.add(word);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void step1_html2Text(String srcDir, String tarPath) {
		try {
			ArticleSentencesExtractor ase = new ArticleSentencesExtractor();
			File dir = new File(srcDir);
			List<String> articleLineList = new ArrayList<String>();
			int i = 0;
			for (File file : dir.listFiles()) {
				String html = FileUtils.readFileToString(file);
				String text = ase.getText(html);
				String[] sents = OpenNLPSentSplitter.getInstance()
						.splitToArray(text);
				List<String> tokenList = new ArrayList<String>();
				for (String sent : sents) {
					List<String> tokens = OpenNLPTokenizer.getInstance()
							.tokenizeToList(sent);
					tokenList.addAll(tokens);
				}

				System.out.println("第" + (i + 1) + "篇文档的单词数量："
						+ tokenList.size());
				StringBuilder sb = new StringBuilder();
				for (String token : tokenList) {
					token = LingPipeStemmer.getInstance().stem(
							token.toLowerCase());
					sb.append(token).append(" ");
				}
				// articleLineList.add(sb.toString());
				System.out.println("正在处理第" + (++i) + "个文件!");
				articleLineList.add(sb.toString());
			}
			FileUtils.writeLines(new File(tarPath), articleLineList);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (BoilerpipeProcessingException e) {
			e.printStackTrace();
		}
	}

	public static void look() {
		try {
			List<String> lines = FileUtils
					.readLines(new File("oil_lines.data"));
			int i = 0;
			for (String line : lines) {
				if (line.contains("oil")) {
					++i;
				}
			}
			System.out.println("一共" + (++i) + "篇有关石油的文档！");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void step2_removeStopWords(String srcPath, String tarPath) {
		try {
			List<String> lines = FileUtils.readLines(new File(srcPath));
			List<String> newLines = new ArrayList<String>();
			int i = 0;
			for (String line : lines) {
				StringBuilder sb = new StringBuilder();
				String[] tokens = line.split(" ");
				for (String token : tokens) {
					if (!token.contains("(") && !token.contains(")")
							&& !token.contains("/") && !token.contains("\\")
							&& !token.contains("--") && !token.isEmpty()
							&& token.length() > 1
							&& !stopwords.contains(token.toLowerCase())
							&& !StringUtils.isAlphanumeric(token)
							&& !StringUtils.isNumeric(token)) {
						sb.append(token.toLowerCase()).append(" ");
					}
				}
				newLines.add(sb.toString());
				System.out.println("正在处理第" + (++i) + "个文件！");
			}
			FileUtils.writeLines(new File(tarPath), newLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void step3_calcIDF(String notOilPath, String oilPath,
			String tarPath) {
		try {
			List<String> notoilDocs = FileUtils.readLines(new File(notOilPath));
			List<String> oilDocs = FileUtils.readLines(new File(oilPath));
			List<String> allDocs = new ArrayList<String>();
			allDocs.addAll(allDocs);
			allDocs.addAll(notoilDocs);

			Map<String, Double> idfMap = Vectorizer.vectorizeByIDF(allDocs);
			List<String> wordIDFLines = new ArrayList<String>();
			for (String word : idfMap.keySet()) {
				wordIDFLines.add(word + "\t" + idfMap.get(word));
			}

			FileUtils.writeLines(new File(tarPath), wordIDFLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void step4_filterIDF(String srcPath, String tarPath) {
		try {
			List<String> lines = FileUtils.readLines(new File(srcPath));
			List<String> newLines = new ArrayList<String>();
			int i = 0;
			for (String line : lines) {
				String[] items = line.split("\t");
				if (!items[0].isEmpty() && !items[0].startsWith("\"")
						&& !items[0].startsWith("'")
						&& !StringUtils.isAlphanumeric(items[0])
						&& !StringUtils.isNumeric(items[0])
						&& !items[0].contains(",") && !items[0].startsWith("&")
						&& !items[0].contains(".") && !items[0].contains(":")
						&& !items[0].contains("\"") && !items[0].contains("“")
						&& !items[0].contains("”") && !items[0].contains("$")
						&& !items[0].equals("n't")) {
					newLines.add(line);
				}
				System.out.println("正在处理第" + (++i) + "个单词!");
			}
			FileUtils.writeLines(new File(tarPath), newLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void step5_calcTFIDFVector(String srcPath, String tarPath) {
		try {
			List<String> wordIDFLInes = FileUtils.readLines(new File(
					"step4_idf.data"));
			Map<String, Double> idfMap = new HashMap<String, Double>();
			List<String> dict = new ArrayList<String>();
			for (String wordidf : wordIDFLInes) {
				String[] items = wordidf.split("\t");
				idfMap.put(items[0], Double.parseDouble(items[1]));
				dict.add(items[0]);
			}
			List<String> docs = FileUtils.readLines(new File(srcPath));
			int i = 0;
			for (String doc : docs) {
				String[] terms = doc.split(" ");
				Map<String, Double> tfMap = Vectorizer.vectorizeByTF(terms);
				double tfidf = 0.0;
				List<Double> csvLineValues = new ArrayList<Double>();
				for (String word : dict) {
					if (tfMap.containsKey(word)) {
						tfidf = tfMap.get(word) * idfMap.get(word);
					} else {
						tfidf = 0.0;
					}
					csvLineValues.add(tfidf);
				}
				String csvLine = StringUtils.join(csvLineValues.toArray(), ",");
				FileUtils.write(new File(tarPath), csvLine + "\n", true);
				System.out.println("正在处理第" + (++i) + "个文档!");
			}
			FileUtils.writeLines(new File("dict"), dict);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void step6_csv2ARFF(String srcPath1, String srcPath2,
			String tarPath) {
		try {
			int i = 0;
			List<String> newLines = new ArrayList<String>();
			String relation = "@relation oil";
			++i;
			System.out.println("添加" + "@relation oil: " + i);
			newLines.add(relation);
			newLines.add("\n");
			++i;
			System.out.println("添加" + "换行符: " + i);
			List<String> dict = FileUtils.readLines(new File("dict"));
			for (String word : dict) {
				newLines.add("@attribute" + " " + word + " " + "numeric");
				++i;
			}
			System.out.println("添加" + "属性信息: " + i);
			newLines.add("@attribute related {yes, no}");
			++i;
			System.out.println("添加" + "类别信息: " + i);
			newLines.add("\n");
			++i;
			System.out.println("添加" + "换行符: " + i);
			newLines.add("@data");
			++i;
			System.out.println("添加" + "@data 信息: " + i);
			List<String> lines1 = FileUtils.readLines(new File(srcPath1));
			for (String line : lines1) {
				newLines.add(line + "," + "yes");
				++i;
			}
			System.out.println("添加" + "正类: " + i);

			List<String> lines2 = FileUtils.readLines(new File(srcPath2));
			for (String line : lines2) {
				newLines.add(line + "," + "no");
				++i;
			}
			System.out.println("添加" + "反类: " + i);
			FileUtils.writeLines(new File(tarPath), newLines);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		step1_html2Text("train/oil/", "oil_step1.data");
		step1_html2Text("train/notoil/", "notoil_step1.data");
		// look();
		step2_removeStopWords("oil_step1.data", "oil_step2.data");
		step2_removeStopWords("notoil_step1.data", "notoil_step2.data");
		step3_calcIDF("notoil_step2.data", "oil_step2.data", "step3_idf.data");
		step4_filterIDF("step3_idf.data", "step4_idf.data");
		step5_calcTFIDFVector("oil_step2.data", "oil_step5.vector");
		step5_calcTFIDFVector("notoil_step2.data", "notoil_step5.vector");
		step6_csv2ARFF("oil_step5.vector", "notoil_step5.vector", "oil.arff");
		
	}
}
