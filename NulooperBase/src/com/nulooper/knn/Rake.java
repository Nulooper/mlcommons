package com.nulooper.knn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;

/**
 * Java Port of RAKE (Rapid Automatic Keyword Extraction algorithm)
 * implementation in python at (https://github.com/aneesha/RAKE).
 * 
 * Author: AskDrCatcher License: MIT
 */
public class Rake {

	private static Rake instance = new Rake();

	private static final List<String> SMART_STOP_WORDS = new ArrayList<String>();
	static {
		try {
			List<String> lines = FileUtils.readLines(new File(
					"SmartStoplist.txt"));
			for (String line : lines) {
				if (!line.startsWith("#")) {
					SMART_STOP_WORDS.add(line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Rake() {

	}

	public static Rake getInstance() {
		return instance;
	}

	private boolean isNumber(String str) {
		return str.matches("[0-9.]");
	}

	private List<String> separateWords(String text, int minimumWordReturnSize) {

		List<String> separateWords = new ArrayList<String>();
		String[] words = text.split("[^a-zA-Z0-9_\\+\\-/]");

		if (words != null && words.length > 0) {

			for (String word : words) {

				String wordLowerCase = word.trim().toLowerCase();

				if (wordLowerCase.length() > 0
						&& wordLowerCase.length() > minimumWordReturnSize
						&& !isNumber(wordLowerCase)) {

					separateWords.add(wordLowerCase);
				}
			}
		}

		return separateWords;
	}

	private List<String> splitSentences(String text) {

		String[] sentences = text
				.split("[.!?,;:\\t\\\\-\\\\\"\\\\(\\\\)\\\\\\'\\u2019\\u2013]");

		if (sentences != null) {
			return new ArrayList<String>(Arrays.asList(sentences));
		} else {
			return new ArrayList<String>();
		}
	}

	private Pattern buildStopWordRegex() {

		StringBuilder stopWordPatternBuilder = new StringBuilder();
		int count = 0;
		for (String stopWord : SMART_STOP_WORDS) {
			if (count++ != 0) {
				stopWordPatternBuilder.append("|");
			}
			stopWordPatternBuilder.append("\\b").append(stopWord).append("\\b");
		}

		return Pattern.compile(stopWordPatternBuilder.toString(),
				Pattern.CASE_INSENSITIVE);
	}

	private List<String> generateCandidateKeywords(List<String> sentenceList,
			Pattern stopWordPattern) {
		List<String> phraseList = new ArrayList<String>();

		for (String sentence : sentenceList) {

			String sentenceWithoutStopWord = stopWordPattern.matcher(sentence)
					.replaceAll("|");
			String[] phrases = sentenceWithoutStopWord.split("\\|");

			if (null != phrases && phrases.length > 0) {
				for (String phrase : phrases) {
					if (phrase.trim().toLowerCase().length() > 0) {
						phraseList.add(phrase.trim().toLowerCase());
					}
				}
			}
		}

		return phraseList;
	}

	private Map<String, Double> calculateWordScores(List<String> phraseList) {

		Map<String, Integer> wordFrequency = new HashMap<String, Integer>();
		Map<String, Integer> wordDegree = new HashMap<String, Integer>();
		Map<String, Double> wordScore = new HashMap<String, Double>();

		for (String phrase : phraseList) {

			List<String> wordList = separateWords(phrase, 0);
			int wordListLength = wordList.size();
			int wordListDegree = wordListLength - 1;

			for (String word : wordList) {

				if (!wordFrequency.containsKey(word)) {
					wordFrequency.put(word, 0);
				}

				if (!wordDegree.containsKey(word)) {
					wordDegree.put(word, 0);
				}

				wordFrequency.put(word, wordFrequency.get(word) + 1);
				wordDegree.put(word, wordDegree.get(word) + wordListDegree);
			}
		}

		Iterator<String> wordIterator = wordFrequency.keySet().iterator();

		while (wordIterator.hasNext()) {
			String word = wordIterator.next();

			wordDegree
					.put(word, wordDegree.get(word) + wordFrequency.get(word));

			if (!wordScore.containsKey(word)) {
				wordScore.put(word, 0.0);
			}

			wordScore.put(word, wordDegree.get(word)
					/ (wordFrequency.get(word) * 1.0));
		}

		return wordScore;
	}

	public Map<String, Double> generateCandidateKeywordScores(
			List<String> phraseList, Map<String, Double> wordScore) {

		Map<String, Double> keyWordCandidates = new HashMap<String, Double>();

		for (String phrase : phraseList) {

			List<String> wordList = separateWords(phrase, 0);
			double candidateScore = 0;

			for (String word : wordList) {
				candidateScore += wordScore.get(word);
			}

			keyWordCandidates.put(phrase, candidateScore);
		}

		return keyWordCandidates;
	}

	public List<KeywordPair> getKeywordCandidates(String text, int topN) {
		if (null == text || text.isEmpty() || topN < 0) {
			return Collections.emptyList();
		}
		List<String> sentenceList = splitSentences(text);
		Pattern stopWordPattern = buildStopWordRegex();
		List<String> phraseList = generateCandidateKeywords(sentenceList,
				stopWordPattern);
		Map<String, Double> wordScore = calculateWordScores(phraseList);
		List<KeywordPair> keywordPairs = new ArrayList<KeywordPair>();
		for (String word : wordScore.keySet()) {
			keywordPairs.add(new KeywordPair(word, wordScore.get(word)));
		}
		Collections.sort(keywordPairs, new Comparator<KeywordPair>() {

			@Override
			public int compare(KeywordPair kp1, KeywordPair kp2) {
				return Double.compare(kp2.score, kp1.score);
			}

		});

		int topK = topN;
		if (topN < keywordPairs.size()) {
			topK = topN;
		} else {
			topK = keywordPairs.size();
		}

		return keywordPairs.subList(0, topK);
	}

	public static void main(String[] args) throws IOException {

		// String text =
		// "IJobs: South Africa: IOL Jobs Job Listings Contact US Feedback About IOL Jobs My Account Job opportunities in South Africa with IOL Jobs. 2,305 Jobs in South Africa Search What (job title, reference or keywords) Where (filter by location) All Locations South Africa - Other International Eastern Cape - All Locations Eastern Cape - Aliwal North Eastern Cape - East London Eastern Cape - Port Elizabeth Eastern Cape - Eastern Cape Other Free State - All Locations Free State - Bethlehem Free State - Bloemfontein Free State - Ficksburg Free State - Harrismith Free State - Sasolburg Free State - Virginia Free State - Welkom Free State - Other Gauteng - All Locations Gauteng - Johannesburg Gauteng - Johannesburg North Gauteng - Johannesburg South Gauteng - Johannesburg East Gauteng - Johannesburg West Gauteng - Midrand Gauteng - Pretoria Gauteng - Centurion Gauteng - other Kwazulu Natal - All Locations Kwazulu Natal - Central / Southern Drakensberg Kwazulu Natal - Durban Kwazulu Natal - Midlands Kwazulu Natal - North Coast Kwazulu Natal - South Coast Kwazulu Natal - Zululand Kwazulu Natal - Other Limpopo - All Locations Limpopo - Pietersburg Limpopo - Polokwane Limpopo - Tzaneen Limpopo - other Mpumalanga - All Locations Mpumalanga - Ermelo Mpumalanga - Hazyview Mpumalanga - Middelburg Mpumalanga - Nelspruit Mpumalanga - Witbank Mpumalanga - Other North West - All Locations North West - Klerksdorp North West - Lichtenburg North West - Mafikeng North West - Potchefstroom North West - Rustenburg North West - Vryburg North West - Other Northern Cape - All Locations Northern Cape - Calvinia Northern Cape - Colesberg Northern Cape - De Aar Northern Cape - Kimberley Northern Cape - Prieska Northern Cape - Upington Northern Cape - Other Western Cape - All Locations Western Cape - Boland Western Cape -Cape Town Centre Western Cape - Garden Route Western Cape - Karoo Western Cape - Overberg Western Cape - West Coast Western Cape - Southern Suburbs Western Cape - Northern Suburbs Western Cape - Other Western Cape - Helderberg Accountancy Banking, Financial Services & Insurance Beauty & Hair Care Jobs Construction & Property Customer Service, Call & Contact Centre Jobs Design & Architecture Education & Training Engineering & Technical Environmental, Health & Safety Jobs Green Jobs Hospitality & Catering HR & Recruitment IT and Telecommunications Legal Leisure & Sports Jobs Management & Executive Jobs Manufacturing, Production & Utilities Jobs Marketing, Advertising & PR Media, New Media & Creative jobs Medical & Healthcare Jobs Pharmaceutical Jobs Public Sector Jobs R&D, Science & Scientific Research Retail & Wholesale Jobs Sales & Purchasing Secretarial, Admin & PA Skilled Labour, Trades & Manual Social, Community & Charity Jobs Transportation, Logistics & Supply Chain Travel & Tourism Jobs Eastern Cape Aliwal North East London Port Elizabeth Other Free State Bethlehem Bloemfontein Ficksburg Harrismith Sasolburg Virginia Welkom Other Gauteng Centurion Johannesburg Johannesburg East Johannesburg North Johannesburg South Johannesburg West Midrand Pretoria Other Kwazulu Natal Central / Southern Drakensberg Durban Midlands North Coast South Coast Zululand Other Limpopo Pietersburg Polokwane Tzaneen Other Mpumalanga Ermelo Hazyview Middelburg Nelspruit Witbank Other North West Klerksdorp Lichtenburg Mafikeng Potchefstroom Rustenburg Vryburg Other Northern Cape Kimberley Calvinia Colesberg De Aar Prieska Upington Other Western Cape Boland Garden Route Karoo Overberg West Coast Cape Town Centre Helderberg Northern Suburbs Southern Suburbs Other SALES MANAGERS   Listed Thu 29 May 2014 SALESMANAGERSNationalcompany seeksCall Dawn on:021 948 7198 /078 198 1245 SALES CONSULANTS   Listed Thu 29 May 2014 SALESCONSULANTSNationalcompany seeksCall Dawn on:021 948 7198 /078 198 1245 CANDIDATE ATTORNEY   Listed Thu 29 May 2014 CANDIDATE ATTORNEYRECRUITMENT PROGRAMME(R141 339  R164 028 paA credible employer of choice rooted in leadership, driven by the value and advancement of human rights. A key contributor to South Africas constitutional democracy ... CASHIERS   Listed Thu 29 May 2014 CASHIERSEEDEDAT BOTTLE STORESHomevale & West EndbranchesDROP off CV atOVERLAND LIQUORSHEUWELSIG Grade A,B And C Security Officers   Listed Thu 29 May 2014 With hotel experience. Send Cv's to 7 Foundrey Road, Salt River. ☎ 021 447 726 Cognitive Behavioural Therapy Intervention   Listed Thu 1 May 2014 The University of Cape Towns Addictions Unit is offering a free 6-session Cognitive Behavioural  Therapy intervention as part of a research study. We are looking for adults 18 and older who are  addicted to TIK/Methamphetamine and who ... Featured Jobs Job Title Company & Location Salary   CellC RICA Agent Listed Fri 16 May 2014 LikeMinds Kwazulu Natal - Midlands Less than R10,000 / Year   Cutter Listed Fri 16 May 2014 Elledi Creations Westen Cape -Cape Town Centre Sales Representative Listed Wed 21 May 2014 Kirk Marketing (PTY) Ltd Gauteng - Johannesburg North R240,001-260,000 / Year   Domestic Worker Listed Thu 22 May 2014 Anmarie Dexter Northern Cape - Kimberley   YOUTH SALES CONSULTANTS Listed Fri 23 May 2014 RECRUIT 5 HOLDINGS Gauteng - other R70,001-80,000 / Year   Quadriplegic's Assistant Listed Wed 28 May 2014 Market Immersion Gauteng - Johannesburg North R40,001-50,000 / Year Click here to download the government Z83 employment application forms Get Email Alerts every time a job of your interest is advertised Advertise with us For exceptional offers in print and on-line - place an ad today. Share or Recomend Facebook Twitter No Iframes ©2012 Independent Online. All rights strictly reserved. Independent Online is a wholly owned subsidiary of Independent News & Media. Reliance on the information this site contains is at your own risk. Please read our Terms and Conditions of Use and Privacy Policy. Contact us here ";
		String text = "";
		List<KeywordPair> kps = Rake.getInstance().getKeywordCandidates(text,
				10);
		for (KeywordPair kp : kps) {
			System.out.println(kp);
		}
	}
}