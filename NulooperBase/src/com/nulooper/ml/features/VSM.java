package com.nulooper.ml.features;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

public class VSM {
	private Set<String> vocabulary;
	private Map<String, Map<String, Double>> tfidfMatrix;

	public Set<String> getVocabulary() {
		return Collections.unmodifiableSet(this.vocabulary);
	}

	public Map<String, Map<String, Double>> getTfidfMatrix() {
		return Collections.unmodifiableMap(this.tfidfMatrix);
	}
}
