package com.nulooper.ml.features;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Vectorizer {

	public static Map<String, Integer> vectorizeByCount(String[] tokens) {
		if (null == tokens || tokens.length == 0) {
			return Collections.emptyMap();
		}

		Map<String, Integer> wordCountMap = new HashMap<String, Integer>();
		for (String token : tokens) {
			if (wordCountMap.containsKey(token)) {
				wordCountMap.put(token, wordCountMap.get(token) + 1);
			} else {
				wordCountMap.put(token, 1);
			}
		}

		return Collections.unmodifiableMap(wordCountMap);
	}

	public static Map<String, Integer> vectorizeByCount(List<String> tokenList) {
		if (null == tokenList || tokenList.isEmpty()) {
			return Collections.emptyMap();
		}

		String[] tokens = new String[tokenList.size()];
		tokenList.toArray(tokens);

		return vectorizeByCount(tokens);
	}

	public static Map<String, Double> vectorizeByTF(String[] tokens) {
		if (null == tokens || tokens.length == 0) {
			return Collections.emptyMap();
		}

		Map<String, Integer> wordCountMap = vectorizeByCount(tokens);
		int length = tokens.length;
		Map<String, Double> wordTFMap = new HashMap<String, Double>();
		for (String key : wordCountMap.keySet()) {
			wordTFMap.put(key, wordCountMap.get(key) * 1.0 / length);
		}

		return Collections.unmodifiableMap(wordTFMap);
	}

	public static Map<String, Double> vectorizeByTF(List<String> tokenList) {
		if (null == tokenList || tokenList.isEmpty()) {
			return Collections.emptyMap();
		}

		String[] tokens = new String[tokenList.size()];

		return vectorizeByTF(tokens);
	}

	public static Map<String, Double> vectorizeByIDF(String[] tokens,
			Map<String, Double> wordIDFMap) {
		return null;
	}

	public static Map<String, Double> vectorizeByIDF(List<String> tokenList,
			Map<String, Double> wordIDFMap) {
		return null;
	}

	/**
	 * 
	 * 
	 * @param documents
	 *            文档列表，list中的每一个元素代表一篇文档，单词使用空格隔开.
	 * @return
	 */
	public static Map<String, Double> vectorizeByIDF(List<String> documents) {
		Map<String, Double> idfMap = new HashMap<String, Double>();
		int docCount = documents.size();
		for (String doc : documents) {
			String[] tokens = doc.split(" ");
			Set<String> set = new HashSet<String>();
			for (String token : tokens) {
				set.add(token);
			}
			for (String token : set) {
				if (idfMap.containsKey(token)) {
					idfMap.put(token, idfMap.get(token) + 1.0);
				} else {
					idfMap.put(token, 1.0);
				}
			}
		}

		for (String token : idfMap.keySet()) {
			idfMap.put(token, Math.log(docCount * 1.0 / idfMap.get(token)));
		}

		return Collections.unmodifiableMap(idfMap);
	}

	public static void main(String[] args) {
		String sent1 = "I love you you !";
		sent1 = sent1.toLowerCase();
		String sent2 = "Do you love me ?";
		sent2 = sent2.toLowerCase();
		Map<String, Double> sent1TF = vectorizeByTF(sent2.split(" "));
		for (String word : sent1TF.keySet()) {
			// System.out.println(word + ":\t" + sent1TF.get(word));
		}

		List<String> docs = new ArrayList<String>();
		docs.add(sent1);
		docs.add(sent2);
		Map<String, Double> idfMap = vectorizeByIDF(docs);
		for (String word : idfMap.keySet()) {
			System.out.println(word + ":\t" + idfMap.get(word));
		}
	}
}
