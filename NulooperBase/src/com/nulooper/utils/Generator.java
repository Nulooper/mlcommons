package com.nulooper.utils;

import java.util.List;

public class Generator {
	public static void generateCSV(List<List<Double>> matrix,
			List<String> featureNames, String tarPath) {

	}

	public static void generateCSV(double[][] matrix,
			List<String> featureNames, String tarPath) {

	}

	public static void generateArff(List<List<Double>> matrix,
			List<String> featureNames, String tarPath) {

	}

	public static void generateArff(double[][] matrix,
			List<String> featureNames, String tarPath) {

	}

	public static void generateTSV(List<List<Double>> matrix,
			List<String> featureNames, String tarPath) {

	}

	public static void generateTSV(double[][] matrix,
			List<String> featureNames, String tarPath) {
	}

	/**
	 * 生成Mallet CRF格式特征文件.
	 * 
	 * @param tarPath
	 */
	public static void generateCRF(String tarPath) {

	}

	/**
	 * 生成SVM格式特征文件.
	 * 
	 * @param tarPath
	 */
	public static void generateSVM(String tarPath) {

	}

	/**
	 * 生成最大熵模型特征格式文件.
	 * 
	 * @param tarPath
	 */
	public static void generateMEM(String tarPath) {

	}
}
