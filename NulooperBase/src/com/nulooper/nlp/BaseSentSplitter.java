package com.nulooper.nlp;

import java.util.List;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 */
public interface BaseSentSplitter {

	public List<String> splitToList(String inputText);

	public String[] splitToArray(String inputText);
}
