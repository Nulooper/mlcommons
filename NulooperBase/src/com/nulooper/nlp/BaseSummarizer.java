package com.nulooper.nlp;

import java.util.List;

public interface BaseSummarizer {
	public List<String> summarizeToList(String inputText, double rate);

	public String[] summarizeToArray(String inputtext, double rate);
}
