package com.nulooper.nlp;

import java.util.List;

public interface BaseChunker {
	public List<String> chunkToList(String inputSentence);

	public String[] chunkToArray(String inputSentence);

	public List<String> getNPChunkList(String inputSentence);

	public String[] getNPChunkArray(String inputSentence);

	public List<String> getVPChunkList(String inputSentence);

	public String[] getVPChunkArray(String inputSentence);

}
