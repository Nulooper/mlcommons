package com.nulooper.nlp;

import java.util.List;

public interface BaseKeysExtractor {
	public List<String> extractToList(String inputText, int topK);

	public String[] extractToArray(String inputText, int topK);
}
