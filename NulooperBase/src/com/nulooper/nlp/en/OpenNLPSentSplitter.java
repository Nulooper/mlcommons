package com.nulooper.nlp.en;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.nulooper.nlp.BaseSentSplitter;

import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;

/**
 * This class is for splitting sentence.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014/05/13
 * 
 */
public class OpenNLPSentSplitter implements BaseSentSplitter {

	private static final String OPENNLP_EN_SENT = "resources/models/opennlp/en-sent.bin";

	private static OpenNLPSentSplitter instance = null;
	private SentenceDetectorME sentenceDetector;

	public OpenNLPSentSplitter() {
		InputStream modelIn = null;
		try {
			modelIn = new FileInputStream(OPENNLP_EN_SENT);
			SentenceModel model = new SentenceModel(modelIn);
			this.sentenceDetector = new SentenceDetectorME(model);
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (null != modelIn) {
				try {
					modelIn.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static OpenNLPSentSplitter getInstance() {
		if (null == instance) {
			instance = new OpenNLPSentSplitter();
		}

		return instance;
	}

	@Override
	public List<String> splitToList(String inputText) {
		if (inputText == null || inputText.isEmpty()) {
			return Collections.emptyList();
		}

		String[] sents = this.sentenceDetector.sentDetect(inputText);
		if (null == sents) {
			return Collections.emptyList();
		}

		List<String> sentenceList = new ArrayList<String>();
		for (String sent : sents) {
			sentenceList.add(sent);
		}

		return Collections.unmodifiableList(sentenceList);
	}

	@Override
	public String[] splitToArray(String inputText) {
		if (null == inputText || inputText.isEmpty()) {
			return new String[0];
		}

		List<String> sentenceList = splitToList(inputText);
		String[] sentence = new String[sentenceList.size()];
		sentenceList.toArray(sentence);

		return sentence;
	}

	public static void main(String[] args) {
		String text = "I love you ! Do you love me ? en.";
		String[] sents = OpenNLPSentSplitter.getInstance().splitToArray(text);
		for (String sent : sents) {
			System.out.println(sent);
		}

	}

}
