package com.nulooper.nlp.en;

import com.nulooper.nlp.BaseLemmatizer;

import edu.stanford.nlp.process.Morphology;

public class StanfordLemmatizer implements BaseLemmatizer {
	private Morphology morphology;

	private static StanfordLemmatizer instance = null;

	public StanfordLemmatizer() {
		this.morphology = new Morphology();
	}

	public static StanfordLemmatizer getInstance() {
		if (null == instance) {
			instance = new StanfordLemmatizer();
		}

		return instance;
	}

	@Override
	public String lemmatize(String inWord, String pos) {
		return this.morphology.lemma(inWord, pos);
	}

	public static void main(String[] args) {
		String word = "BOOKS";
		String pos = "VP";
		System.out.println(StanfordLemmatizer.getInstance()
				.lemmatize(word, pos));
	}
}
