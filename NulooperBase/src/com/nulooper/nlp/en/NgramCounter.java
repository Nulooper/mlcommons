package com.nulooper.nlp.en;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NgramCounter {
	public static List<String> bigramCount(String inputSentence) {
		String[] tokens = inputSentence.split(" ");
		List<String> bigrams = new ArrayList<String>();
		int length = tokens.length;
		int i = 0;
		while (i < length - 1) {
			bigrams.add("[" + tokens[i] + " " + tokens[i + 1] + "]");
			i = i + 1;
		}
		return Collections.unmodifiableList(bigrams);
	}

	
}
