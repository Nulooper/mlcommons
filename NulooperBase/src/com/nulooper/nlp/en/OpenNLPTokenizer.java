package com.nulooper.nlp.en;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.nulooper.nlp.BaseTokenizer;

import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014/05/01
 */
public class OpenNLPTokenizer implements BaseTokenizer {

	private static final String OPENNLP_EN_TOKEN = "resources/models/opennlp/en-token.bin";

	private static OpenNLPTokenizer instance = null;
	private Tokenizer tokenizer;

	public OpenNLPTokenizer() {
		InputStream modelIn = null;
		try {
			modelIn = new FileInputStream(OPENNLP_EN_TOKEN);
			TokenizerModel model = new TokenizerModel(modelIn);
			this.tokenizer = new TokenizerME(model);
		} catch (Throwable e) {
			e.printStackTrace();
		} finally {
			if (modelIn != null) {
				try {
					modelIn.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static OpenNLPTokenizer getInstance() {
		if (null == instance) {
			instance = new OpenNLPTokenizer();
		}
		return instance;
	}

	@Override
	public List<String> tokenizeToList(String input) {
		if (input == null || input.isEmpty()) {
			return Collections.emptyList();
		}

		String[] tokens = this.tokenizer.tokenize(input);
		List<String> tokenList = new ArrayList<String>();
		for (String token : tokens) {
			tokenList.add(token);
		}

		return Collections.unmodifiableList(tokenList);
	}

	@Override
	public String[] tokenizeToArray(String input) {
		if (null == input || input.isEmpty()) {
			return new String[0];
		}

		return this.tokenizer.tokenize(input);
	}

	public static void main(String[] args) {
		String text = "I love you !";
		String[] tokens = OpenNLPTokenizer.getInstance().tokenizeToArray(text);
		for (String token : tokens) {
			System.out.println(token);
		}
		String ch = "我想去天安门看升旗!";
		String[] chWords = OpenNLPTokenizer.getInstance().tokenizeToArray(ch);
		for(String word : chWords){
			System.out.println(word);
		}
	}

}
