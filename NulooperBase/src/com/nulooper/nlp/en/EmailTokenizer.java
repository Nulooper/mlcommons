package com.nulooper.nlp.en;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.nulooper.nlp.BaseTokenizer;

/**
 * Created by Wang Zhiwei.
 * 
 * @version 0.0.1
 * @date 2014/4/26
 */
public class EmailTokenizer implements BaseTokenizer {

	private static EmailTokenizer instance = null;

	private EmailTokenizer() {

	}

	public static EmailTokenizer getInstance() {
		if (null == instance) {
			return instance;
		}
		return instance;
	}

	@Override
	public List<String> tokenizeToList(String inputSentence) {

		if ((null == inputSentence) || inputSentence.isEmpty()
				|| !inputSentence.contains("@")) {
			return Collections.emptyList();
		}

		String[] items = inputSentence.split("@");
		String username = items[0];
		String[] tokens = items[1].split("\\.");

		List<String> tokenList = new ArrayList<String>();
		tokenList.add(username);

		for (String token : tokens) {
			tokenList.add(token);
		}

		return Collections.unmodifiableList(tokenList);
	}

	@Override
	public String[] tokenizeToArray(String inputSentence) {
		List<String> tokenList = tokenizeToList(inputSentence);
		String[] tokenArray = new String[tokenList.size()];
		tokenList.toArray(tokenArray);

		return tokenArray;
	}

	public static void main(String[] args) {
	}

}
