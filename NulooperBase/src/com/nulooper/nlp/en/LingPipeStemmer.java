package com.nulooper.nlp.en;

import com.aliasi.tokenizer.PorterStemmerTokenizerFactory;
import com.nulooper.nlp.BaseStemmer;

public class LingPipeStemmer implements BaseStemmer {

	private static LingPipeStemmer instance = null;

	public LingPipeStemmer() {

	}

	public static LingPipeStemmer getInstance() {
		if (null == instance) {
			instance = new LingPipeStemmer();
		}
		return instance;
	}

	@Override
	public String stem(String inWord) {
		if (inWord == null || inWord.isEmpty())
			return "";
		return PorterStemmerTokenizerFactory.stem(inWord);
	}

}
