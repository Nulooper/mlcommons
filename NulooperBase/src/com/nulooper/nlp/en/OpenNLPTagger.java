package com.nulooper.nlp.en;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.nulooper.nlp.BaseTagger;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;

public class OpenNLPTagger implements BaseTagger {

	private static final String OPENNLP_EN_POS_MAXENT = "resources/models/opennlp/en-pos-maxent.bin";

	private static OpenNLPTagger instance = null;
	private POSTaggerME tagger;

	public OpenNLPTagger() {
		InputStream modelIn = null;
		try {
			modelIn = new FileInputStream(OPENNLP_EN_POS_MAXENT);
			POSModel model = new POSModel(modelIn);
			this.tagger = new POSTaggerME(model);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (modelIn != null) {
				try {
					modelIn.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static OpenNLPTagger getInstance() {
		if (null == instance) {
			instance = new OpenNLPTagger();
		}
		return instance;
	}

	@Override
	public String[] tagToArray(String inputSentence) {

		if (null == inputSentence || inputSentence.isEmpty()) {
			return new String[0];
		}

		String[] sentence = OpenNLPTokenizer.getInstance().tokenizeToArray(
				inputSentence);
		String[] tags = this.tagger.tag(sentence);

		return tags;
	}

	@Override
	public List<String> tagToList(String inputSentence) {
		if (null == inputSentence || inputSentence.isEmpty()) {
			return Collections.emptyList();
		}

		return Arrays.asList(this.tagToArray(inputSentence));
	}

	public static void main(String[] args) throws IOException {
		String sent = "I love you !";
		List<String> tags = OpenNLPTagger.getInstance().tagToList(sent);
		for (String tag : tags) {
			System.out.println(tag);
		}
	}

}
