package com.nulooper.nlp.en;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import opennlp.tools.namefind.NameFinderME;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.util.Span;

public class OpenNLPNER {
	private static OpenNLPNER instance;

	private NameFinderME nameFinder;

	private OpenNLPNER() {
		InputStream modelIn = null;
		try {
			modelIn = new FileInputStream(
					"resources/models/opennlp/en-ner-location.bin");
			TokenNameFinderModel model = new TokenNameFinderModel(modelIn);
			this.nameFinder = new NameFinderME(model);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (modelIn != null) {
				try {
					modelIn.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static OpenNLPNER getInstance() {
		if (null == instance) {
			instance = new OpenNLPNER();
		}
		return instance;
	}

	/**
	 * 
	 * @param tokens
	 *            A sentence made of tokens.
	 * @return names, each name consists of tokens, a space will be between each
	 *         pair of tokens.
	 */
	public String[] findNamesToArray(String[] tokens) {
		if (null == tokens || tokens.length == 0) {
			return new String[0];
		}

		Span[] nameSpans = this.nameFinder.find(tokens);
		String[] names = new String[nameSpans.length];
		int length = names.length;
		System.out.println(names.length);

		int start, end;
		StringBuilder name = new StringBuilder();
		Span span;
		for (int i = 0; i < length; ++i) {
			span = nameSpans[i];
			start = span.getStart();
			end = span.getEnd();
			int j;
			for (j = start; j < end - 1; ++j) {
				name.append(tokens[i]).append(" ");
			}
			name.append(tokens[j]);
			names[i] = name.toString();
		}
		return names;
	}

	public List<String> findNamesToList(String[] tokens) {
		if (null == tokens || tokens.length == 0) {
			return Collections.emptyList();
		}
		String[] names = findNamesToArray(tokens);

		return Arrays.asList(names);
	}

	public static void main(String[] args) {
		String[] tokens = new String[] { "I", "love", "Beijing",
				"!" };
		String[] names = OpenNLPNER.getInstance().findNamesToArray(tokens);
		for (String name : names) {
			System.out.println(name);
		}
	}
}
