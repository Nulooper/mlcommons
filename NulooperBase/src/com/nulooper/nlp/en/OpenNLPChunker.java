package com.nulooper.nlp.en;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.nulooper.nlp.BaseChunker;

import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;

/**
 * This class is for chunking english text.
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014/05/13
 * 
 */
public class OpenNLPChunker implements BaseChunker {
	private static final String OPENNLP_EN_CHUNKER = "resources/models/opennlp/en-chunker.bin";

	private static OpenNLPChunker instance = null;
	private ChunkerME chunker;

	public OpenNLPChunker() {
		InputStream modelIn = null;
		ChunkerModel model = null;
		try {
			modelIn = new FileInputStream(OPENNLP_EN_CHUNKER);
			model = new ChunkerModel(modelIn);
			this.chunker = new ChunkerME(model);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (modelIn != null) {
				try {
					modelIn.close();
				} catch (IOException e) {
				}
			}
		}
	}

	public static OpenNLPChunker getInstance() {
		if (null == instance) {
			instance = new OpenNLPChunker();
		}
		return instance;
	}

	@Override
	public List<String> chunkToList(String inputSentence) {
		String[] chunks = chunkToArray(inputSentence);
		List<String> chunkList = new ArrayList<String>();
		for (String chunk : chunks) {
			chunkList.add(chunk);
		}
		return Collections.unmodifiableList(chunkList);
	}

	@Override
	public String[] chunkToArray(String inputSentence) {
		if (null == inputSentence || inputSentence.isEmpty()) {
			return new String[0];
		}

		String[] sent = OpenNLPTokenizer.getInstance().tokenizeToArray(
				inputSentence);
		String[] pos = OpenNLPTagger.getInstance().tagToArray(inputSentence);

		String[] chunks = this.chunker.chunk(sent, pos);

		return chunks;
	}

	@Override
	public List<String> getNPChunkList(String inputSentence) {
		if (null == inputSentence || inputSentence.isEmpty()) {
			return Collections.emptyList();
		}
		String[] chunkMarks = chunkToArray(inputSentence);
		List<String> npMarkList = new ArrayList<String>();
		List<Integer> npIndexList = new ArrayList<Integer>();
		int length = chunkMarks.length;
		String mark = null;
		for (int i = 0; i < length; ++i) {
			mark = chunkMarks[i];
			if (mark.endsWith("NP")) {
				npMarkList.add(mark);
				npIndexList.add(i);
			}
		}

		List<String> tokenList = OpenNLPTokenizer.getInstance().tokenizeToList(
				inputSentence);
		List<String> chunkList = this.getChunkList(npMarkList, npIndexList,
				tokenList);

		return Collections.unmodifiableList(chunkList);
	}

	@Override
	public String[] getNPChunkArray(String inputSentence) {
		return null;
	}

	@Override
	public List<String> getVPChunkList(String inputSentence) {
		return null;
	}

	@Override
	public String[] getVPChunkArray(String inputSentence) {
		return null;
	}

	private List<String> getChunkList(List<String> markList,
			List<Integer> indexList, List<String> tokens) {
		if (markList == null || indexList == null || tokens == null
				|| markList.isEmpty() || indexList.isEmpty()
				|| tokens.isEmpty()) {
			return Collections.emptyList();
		}
		int size = markList.size();

		String current = markList.get(0);
		String last = current;
		StringBuilder chunkSB = new StringBuilder();
		chunkSB.append(tokens.get(indexList.get(0))).append(" ");
		List<String> chunks = new ArrayList<String>();
		for (int i = 1; i < size; ++i) {
			current = markList.get(i);
			if (last.startsWith("B")) {
				if (current.startsWith("B")) {
					chunks.add(chunkSB.toString());
					chunkSB = new StringBuilder();
					last = current;
					chunkSB.append(tokens.get(indexList.get(i))).append(" ");
				} else if (current.startsWith("I")) {
					chunkSB.append(tokens.get(indexList.get(i))).append(" ");
				}
			} else if (last.startsWith("I")) {
				if (current.startsWith("B")) {
					chunks.add(chunkSB.toString());
					chunkSB = new StringBuilder();
					last = current;
					chunkSB.append(tokens.get(indexList.get(i))).append(" ");
				} else if (current.startsWith("I")) {
					chunkSB.append(tokens.get(indexList.get(i))).append(" ");
				}
			}
		}
		if (current.startsWith("I")) {
			chunks.add(chunkSB.toString());
		}
		return Collections.unmodifiableList(chunks);
	}

	public static void main(String[] args) throws IOException {
		String test = "I love Peking University and oil.";
		List<String> nps = OpenNLPChunker.getInstance().getNPChunkList(test);
		for (String np : nps) {
			System.out.println(np);
		}
	}

}
