package com.nulooper.nlp;

public interface BaseLemmatizer {
	public String lemmatize(String inWord, String pos);
}
