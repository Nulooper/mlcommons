package com.nulooper.nlp;

public interface BaseStemmer {
	public String stem(String inWord);
}
