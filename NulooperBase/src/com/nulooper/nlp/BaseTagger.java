package com.nulooper.nlp;

import java.util.List;

public interface BaseTagger {
	public List<String> tagToList(String inputSentence);

	public String[] tagToArray(String inputSentence);
}
