package org.apache.nutch.indexer.date;

import java.util.List;

/**
 * Created by Wang Zhiwei.
 * 
 * @version 0.0.1
 * @date 2014/4/26
 */
public interface BaseTokenizer {
	public List<String> tokenizeToList(String inputSentence);

	public String[] tokenizeToArray(String inputSentence);
}
