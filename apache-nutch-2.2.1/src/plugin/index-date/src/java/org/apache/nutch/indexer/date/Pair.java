package org.apache.nutch.indexer.date;

public class Pair {
	public String word;
	public double value;

	public Pair(String word, double value) {
		super();
		this.word = word;
		this.value = value;
	}

	@Override
	public String toString() {
		return word + "\t" + value;
	}

}
