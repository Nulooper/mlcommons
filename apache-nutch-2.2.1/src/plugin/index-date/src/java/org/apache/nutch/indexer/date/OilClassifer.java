package org.apache.nutch.indexer.date;

import java.io.File;
import java.io.StringReader;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import weka.classifiers.bayes.NaiveBayes;
import weka.core.Instances;

import org.apache.nutch.indexer.date.OpenNLPTokenizer;

public class OilClassifer {
	private static final String NAIVE_BAYES_MODEL_PATH = "resources/oil_bayes.model";
	private static final String DICT_PATH = "resources/dict";

	private static OilClassifer instance = new OilClassifer();

	private NaiveBayes nb = null;
	private List<String> dict = null;
	private Map<String, Double> idfMap = null;

	private OilClassifer() {
		try {
			nb = (NaiveBayes) weka.core.SerializationHelper
					.read(NAIVE_BAYES_MODEL_PATH);
			dict = FileUtils.readLines(new File(DICT_PATH));

			List<String> idfLines = FileUtils.readLines(new File(
					"resources/idf.data"));
			idfMap = new HashMap<String, Double>();
			for (String line : idfLines) {
				String[] twoItems = line.split("\t");
				idfMap.put(twoItems[0], Double.parseDouble(twoItems[1]));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static OilClassifer getInstance() {
		return instance;
	}

	public String classify(String text) throws Exception {
		if (null == text || null == nb || text.isEmpty()) {
			return "no";
		}
		text = text.toLowerCase();
		String[] tokens = OpenNLPTokenizer.getInstance().tokenizeToArray(text);
		Map<String, Double> tfMap = vectorizeByTF(tokens);

		// create arff format.
		String header = getArffHeader();
		StringBuilder dataSB = new StringBuilder();
		double tfidf = 0.0;
		for (String word : dict) {
			if (tfMap.containsKey(word)) {
				tfidf = tfMap.get(word) * idfMap.get(word);
			} else {
				tfidf = 0.0;
			}
			dataSB.append(tfidf).append(",");
		}
		dataSB.append("no");

		StringBuilder arffSB = new StringBuilder();
		arffSB.append(header).append(dataSB.toString());

		StringReader sr = new StringReader(arffSB.toString());
		Instances data = new Instances(sr);
		if (data.classIndex() == -1)
			data.setClassIndex(data.numAttributes() - 1);

		double clsLabel = nb.classifyInstance(data.firstInstance());
		String label = data.classAttribute().value((int) clsLabel);

		return label;
	}

	private String getArffHeader() {
		StringBuilder headSB = new StringBuilder();
		headSB.append("@relation oil").append("\n");
		for (String word : dict) {
			headSB.append("@attribute" + " " + word + " " + "numeric").append(
					"\n");
		}
		headSB.append("@attribute related {yes, no}").append("\n");
		headSB.append("\n");
		headSB.append("@data").append("\n");

		return headSB.toString();
	}

	private Map<String, Double> vectorizeByTF(String[] tokens) {
		if (null == tokens || tokens.length == 0) {
			return Collections.emptyMap();
		}

		Map<String, Integer> wordCountMap = vectorizeByCount(tokens);
		int length = tokens.length;
		Map<String, Double> wordTFMap = new HashMap<String, Double>();
		for (String key : wordCountMap.keySet()) {
			wordTFMap.put(key, wordCountMap.get(key) * 1.0 / length);
		}

		return Collections.unmodifiableMap(wordTFMap);
	}

	private Map<String, Integer> vectorizeByCount(String[] tokens) {
		if (null == tokens || tokens.length == 0) {
			return Collections.emptyMap();
		}

		Map<String, Integer> wordCountMap = new HashMap<String, Integer>();
		for (String token : tokens) {
			if (wordCountMap.containsKey(token)) {
				wordCountMap.put(token, wordCountMap.get(token) + 1);
			} else {
				wordCountMap.put(token, 1);
			}
		}

		return Collections.unmodifiableMap(wordCountMap);
	}

	public static void main(String[] args) throws Exception {
		String text = "I love oil !";
		String label = OilClassifer.getInstance().classify(text);
		System.out.println(label);
	}
}
