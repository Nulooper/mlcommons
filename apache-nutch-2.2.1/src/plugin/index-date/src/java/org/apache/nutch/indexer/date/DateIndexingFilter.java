package org.apache.nutch.indexer.date;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.nutch.indexer.IndexingException;
import org.apache.nutch.indexer.IndexingFilter;
import org.apache.nutch.indexer.NutchDocument;
import org.apache.nutch.util.TableUtil;
import org.apache.nutch.storage.WebPage;
import org.apache.nutch.storage.WebPage.Field;
import org.apache.solr.common.util.DateUtil;
import org.apache.commons.io.FileUtils;
import org.codehaus.jackson.map.AnnotationIntrospector.Pair;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import de.l3s.boilerpipe.extractors.ArticleSentencesExtractor;
import org.apache.nutch.indexer.date.OilClassifer;

import org.apache.nutch.indexer.date.KeywordPair;
import org.apache.nutch.indexer.date.Rake;


/**
 * 
 * @author Wang Zhiwei
 * @version 0.0.1
 * @date 2014/05/16
 * 
 */
public class DateIndexingFilter implements IndexingFilter {
	private Configuration conf;

	private static final Collection<WebPage.Field> FIELDS = new HashSet<WebPage.Field>();
	static {
		FIELDS.add(WebPage.Field.TEXT);
	}

	@Override
	public Collection<Field> getFields() {
		return FIELDS;
	}

	@Override
	public Configuration getConf() {
		return this.conf;
	}

	@Override
	public void setConf(Configuration arg0) {
		this.conf = arg0;
	}

	@Override
	public NutchDocument filter(NutchDocument doc, String url, WebPage page){

		//Add fetched time.
		LocalDateTime dt = new LocalDateTime();
		String fetchedTime = dt.toDateTime(DateTimeZone.UTC).toString();
		doc.add("fetchedTime", fetchedTime);

		String text = TableUtil.toString(page.getText());

		//Summarise.
		String summary = summarise(text);
		doc.add("summary", summary);


		//Classification.
		String label = "no";
		String title = TableUtil.toString(page.getTitle());
		title = title.toLowerCase();
		List<String> titleWords = OpenNLPTokenizer.getInstance().tokenizeToList(title);
		List<String> textWords = OpenNLPTokenizer.getInstance().tokenizeToList(text);
		if(titleWords.contains("oil") || titleWords.contains("gas") || titleWords.contains("energy") || textWords.contains("oil") || textWords.contains("gas") || textWords.contains("energy")){
			label = "yes";
		}else{
			try{
				label = OilClassifer.getInstance().classify(text);
			}catch(Exception e){

			}	
		}
		
		doc.add("oil", label);
		
		
		//Extract key words.
		List<KeywordPair> kps = Rake.getInstance().getKeywordCandidates(text,
				10);
		StringBuilder sb = new StringBuilder();
		for(KeywordPair kp : kps){
			sb.append(kp.word).append(" ");
		}
		doc.add("keywords", sb.toString());

		return doc;
	}

	private String extractText(String html) throws Throwable {
		ArticleSentencesExtractor ase = new ArticleSentencesExtractor();
		return ase.getText(html);
	}

	private String summarise(String text) {
		if (null == text || text.isEmpty()) {
			return "";
		}
		SimpleSummariser sum = new SimpleSummariser();
		return sum.summarise(text, 2);
	}

}
