package org.apache.nutch.indexer.date;

public class KeywordPair {
	public String word;
	public double score;

	public KeywordPair(String word, double score) {
		this.word = word;
		this.score = score;
	}

	@Override
	public String toString() {
		return word + ", " + score;
	}

}
